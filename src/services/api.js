import axios from "axios";

const api = axios.create({
  baseURL: "http://6aff7a1d.ngrok.io/bon-appetit/api/v1",
});

export default api;
